import QtQuick 2.9
import QtQuick.Layouts 1.3
import Ubuntu.Components 1.3

Page {
    id: aboutPage
    header: PageHeader {
        id: header
        title: i18n.tr('About')
    }

    Flickable {
        anchors {
            top: header.bottom
            right: parent.right
            left: parent.left
            bottom: parent.bottom
        }
        contentHeight: column.height + units.gu(4)
        clip: true

        ColumnLayout {
            id: column
            anchors {
                top: parent.top
                right: parent.right
                left: parent.left
                margins: units.gu(2)
            }

            spacing: units.gu(2)

            Item {
                Layout.fillWidth: true
                Layout.preferredHeight: ubuntuShapeIcon.height

                UbuntuShape {
                    id: ubuntuShapeIcon
                    anchors.centerIn: parent

                    width: units.gu(10)
                    height: units.gu(10)

                    image: Image {
                        source: Qt.resolvedUrl('../assets/logo.png')

                        sourceSize {
                            width: ubuntuShapeIcon.width
                            height: ubuntuShapeIcon.height
                        }
                    }
                }
            }

            Label {
                Layout.fillWidth: true

                text: i18n.tr('Audio Fennec')
                horizontalAlignment: Label.AlignHCenter
            }

            Label {
                Layout.fillWidth: true

                text: i18n.tr('Simple audiocast downloader for Ubuntu Touch')
                horizontalAlignment: Text.AlignHCenter
                wrapMode: Text.WordWrap
            }

            Label {
                Layout.fillWidth: true

                text: i18n.tr('A Brian Douglass app, consider donating if you like it and want to see more apps like it!')
                horizontalAlignment: Text.AlignHCenter
                wrapMode: Text.WordWrap
            }

            Button {
                Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter

                text: i18n.tr('Donate')
                color: UbuntuColors.orange
                onClicked: Qt.openUrlExternally('https://liberapay.com/bhdouglass')
            }
        }
    }
}
