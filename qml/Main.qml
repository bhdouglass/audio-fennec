/*
 * Copyright (C) 2019  Brian Douglass
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * ubuntu-calculator-app is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Ubuntu.Components 1.3
import QtQuick.Layouts 1.3
import io.thp.pyotherside 1.3

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'audio-fennec.bhdouglass'
    automaticOrientation: true

    width: units.gu(45)
    height: units.gu(75)

    Python {
        id: python
        property bool ready: false

        signal updating(string shortname)
        signal downloading(string url, var feed)

        Component.onCompleted: {
            addImportPath(Qt.resolvedUrl('../src/'));

            importModule('audio_fennec', function() {
                ready = true;
            });

            setHandler('updating', function(shortname) {
                updating(shortname);
            });

            setHandler('downloading', function(url, feed) {
                downloading(url, feed);
            });
        }

        onError: {
            console.log('python error: ' + traceback);
        }
    }

    PageStack {
        id: pageStack

        Component.onCompleted: push(Qt.resolvedUrl('FeedListPage.qml'))
    }
}
