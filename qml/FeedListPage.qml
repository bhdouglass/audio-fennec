import QtQuick 2.7
import QtQuick.Layouts 1.3
import Ubuntu.Components 1.3

Page {
    id: feedListPage

    property var feeds: []
    property bool refreshing: true
    property string updatingShortname: ''

    ListModel {
        id: feedModel
    }

    Connections {
        target: python
        onUpdating: {
            updatingShortname = shortname;
        }
    }

    function setFeeds(feeds) {
        feedModel.clear();
        feedListPage.feeds = JSON.parse(feeds);
        for (var i = 0; i < feedListPage.feeds.length; i++) {
            feedModel.append(feedListPage.feeds[i]);
        }
    }

    function refreshFeeds() {
        refreshing = true;
        feedModel.clear();
        python.call('audio_fennec.feeds', [], function(feeds) {
            setFeeds(feeds);
            refreshing = false;
        });
    }

    function updateFeeds() {
        python.call('audio_fennec.update_feeds', [], function(feeds) {
            setFeeds(feeds);
        });
    }

    function deleteFeed(shortname) {
        python.call('audio_fennec.delete_feed', [shortname], function(feeds) {
            setFeeds(feeds);
        });
    }

    Connections {
        target: python
        onReadyChanged: refreshFeeds()
    }

    Connections {
        target: pageStack
        onCurrentPageChanged: {
            if (pageStack.currentPage == feedListPage && python.ready) {
                refreshFeeds(false);
            }
        }
    }

    header: PageHeader {
        id: header
        title: i18n.tr('Audio Fennec')

        trailingActionBar {
            actions: [
                Action {
                    text: i18n.tr('About')
                    iconName: 'info'

                    onTriggered: pageStack.push(Qt.resolvedUrl('AboutPage.qml'));
                },

                Action {
                    text: i18n.tr('Add')
                    iconName: 'add'

                    onTriggered: pageStack.push(Qt.resolvedUrl('AddFeedPage.qml'))
                },

                Action {
                    text: i18n.tr('Update')
                    iconName: 'view-refresh'

                    onTriggered: updateFeeds()
                }
            ]
        }
    }

    Label {
        anchors.centerIn: parent
        width: parent.width
        visible: !refreshing && feeds.length === 0

        text: i18n.tr('No feeds configured, tap the plus to add some!')

        wrapMode: Label.WordWrap
        horizontalAlignment: Label.AlignHCenter
    }

    ActivityIndicator {
        anchors.centerIn: parent
        visible: running
        running: refreshing && feeds.length === 0
    }

    Flickable {
        anchors {
            top: header.bottom
            right: parent.right
            left: parent.left
            bottom: parent.bottom
        }
        clip: true

        ListView {
            anchors.fill: parent
            model: feedModel

            section.property: 'hasUnlistened'
            section.delegate: Item {
                height: units.gu(4)
                Label {
                    anchors {
                        fill: parent
                        margins: units.gu(2)
                    }

                    textSize: Label.Small
                    text: section == 'true' ? i18n.tr('New Episodes') : i18n.tr('No New Episodes')
                    color: theme.palette.normal.backgroundTertiaryText
                }
            }

            delegate: ListItem {
                height: layout.height

                leadingActions: ListItemActions {
                    actions: Action {
                        iconName: 'delete'
                        text: i18n.tr('Delete Feed')

                        onTriggered: deleteFeed(shortname)
                    }
                }

                ListItemLayout {
                    id: layout
                    title.text: dtitle
                    subtitle.text: hasUnlistened ? i18n.tr('New Episodes') + ': ' + unlistened : ''

                    ProgressionSlot {
                        visible: updatingShortname != shortname
                    }

                    ActivityIndicator {
                        SlotsLayout.position: SlotsLayout.Trailing
                        running: updatingShortname == shortname
                    }
                }

                onClicked: pageStack.push(Qt.resolvedUrl('FeedPage.qml'), {
                    shortname: shortname,
                });
            }
        }
    }
}
