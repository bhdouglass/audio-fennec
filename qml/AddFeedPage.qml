import QtQuick 2.7
import Ubuntu.Components 1.3
import QtQuick.Layouts 1.3
import Ubuntu.Components.Popups 1.3

Page {
    id: addFeedPage

    header: PageHeader {
        id: header
        title: i18n.tr('Add Feed')

        trailingActionBar {
            actions: [
                Action {
                    text: i18n.tr('About')
                    iconName: 'info'

                    onTriggered: pageStack.push(Qt.resolvedUrl('AboutPage.qml'));
                }
            ]
        }
    }

    function shouldSave() {
        if (url.text) {
            python.call('audio_fennec.add_feed', [url.text], function() {
                // TODO handle errors
                pageStack.pop();
            });
        }
    }

    Flickable {
        anchors {
            top: header.bottom
            right: parent.right
            left: parent.left
            bottom: parent.bottom
        }
        contentHeight: column.height + units.gu(4)
        clip: true

        ColumnLayout {
            id: column
            anchors {
                top: parent.top
                right: parent.right
                left: parent.left
                rightMargin: units.gu(1)
                leftMargin: units.gu(1)
                topMargin: units.gu(2)
            }

            spacing: units.gu(2)

            Label {
                text: i18n.tr('Url')
                textSize: Label.Large
            }

            TextField {
                id: url
                onAccepted: shouldSave()
                Layout.fillWidth: true
                // TODO input hints
            }

            Button {
                text: i18n.tr('Save')
                color: UbuntuColors.green

                onClicked: shouldSave()
            }
        }
    }
}
